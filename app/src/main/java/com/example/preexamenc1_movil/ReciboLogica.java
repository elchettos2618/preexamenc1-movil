package com.example.preexamenc1_movil;

import java.io.Serializable;

public class ReciboLogica implements Serializable {

    private int numRecibo, puesto;
    private String nombre;
    private float horasTrabNormal, horasTrabExtras, pagoBase = 200;

    public ReciboLogica() {

        this.numRecibo = 0;
        this.puesto = 0;
        this.nombre = "";
        this.horasTrabNormal = 0;
        this.horasTrabExtras = 0;


    }

    public ReciboLogica( int puesto, float horasTrabNormal, float horasTrabExtras) {

        this.puesto = puesto;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }


    public float calcularSubtotal(){
        float subtotal;
        switch (puesto){
            case 0:
                return subtotal = ((pagoBase*1.2f)+horasTrabNormal)+(horasTrabExtras*(pagoBase*1.2f)*2);
            case 1:
                return subtotal = ((pagoBase*1.5f)+horasTrabNormal)+(horasTrabExtras*(pagoBase*1.5f)*2);
            case 2:
                return subtotal = ((pagoBase*2.0f)+horasTrabNormal)+(horasTrabExtras*(pagoBase*2.0f)*2);
            default:
                return pagoBase;

        }



    }

    public float calcularImpuesto(){
        return calcularSubtotal() * 0.16f;
    }

    public float calcularTotal(){

        return calcularSubtotal() - calcularImpuesto();
    }


}
